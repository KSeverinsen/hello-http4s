// package com.github.kseverinsen.hellohttp4s

// import cats.effect.IO
// import org.http4s._
// import org.http4s.implicits._
// import org.specs2.matcher.MatchResult
// import com.github.kseverinsen.infrastructure.interpreters.InMemoryStore
// import com.github.kseverinsen.domain.types.Person

// class HelloWorldSpec extends org.specs2.mutable.Specification {

//   "HelloWorld" >> {
//     "return 200" >> {
//       uriReturns200()
//     }
//     "return hello world" >> {
//       uriReturnsHelloWorld()
//     }
//   }

//   private[this] val retHelloWorld: Response[IO] = {
//     val getHW = Request[IO](Method.GET, Uri.uri("/hello/world"))

//     val foo = InMemoryStore[IO, Person] extends PersonStoreAlgebra[IO]
//     val store = new InMemoryStore[IO, Person]

//     new HelloWorldService[IO](store).service
//       .orNotFound(getHW)
//       .unsafeRunSync()
//   }

//   private[this] def uriReturns200(): MatchResult[Status] =
//     retHelloWorld.status must beEqualTo(Status.Ok)

//   private[this] def uriReturnsHelloWorld(): MatchResult[String] =
//     retHelloWorld.as[String].unsafeRunSync() must beEqualTo(
//       "{\"message\":\"Hello, world\"}"
//     )
// }
