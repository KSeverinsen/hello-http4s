package com.github.kseverinsen.infrastructure.interpreters

import io.circe.Decoder
import io.circe.Encoder
import io.circe._
import cats.effect.Sync
import com.github.kseverinsen.domain.algebra.StoreAlgebra

import scala.collection.mutable

class InMemoryStore[F[_]: Sync, A](
    implicit decoder: Decoder[A],
    encoder: Encoder[A]
) extends StoreAlgebra[F, String, A, String] {

  val repo: mutable.HashMap[String, Json] =
    mutable.HashMap.empty

  def get(key: String): F[Either[String, A]] = {
    def getter(key: String): Either[String, A] =
      repo
        .get(key)
        .flatMap(a => decoder.decodeJson(a).toOption)
        .toRight(s"NotFound: $key")

    Sync[F].delay(getter(key))
  }

  def set(key: String, value: A): F[Either[String, Unit]] = {
    def setter(key: String, value: A): Either[String, Unit] =
      repo
        .put(key, encoder.apply(value))
        .toRight(s"InsertFailed: $key, $value")
        .map(_ => ())

    Sync[F].delay(setter(key, value))
  }

}
