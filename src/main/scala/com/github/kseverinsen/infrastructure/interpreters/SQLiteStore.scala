package com.github.kseverinsen.infrastructure.interpreters

import cats.effect.Sync
import cats.implicits._
import com.github.kseverinsen.domain.algebra.PersonStoreAlgebra
import com.github.kseverinsen.domain.types.Person
import doobie.implicits._
import doobie.util.query.Query0
import doobie.util.transactor.Transactor
import doobie.util.update.Update0

object PersonStoreDAO {

  val drop =
    sql"""
    DROP TABLE IF EXISTS person
  """.update.run

  val create =
    sql"""
    CREATE TABLE person (
      key VARCHAR(25) UNIQUE PRIMARY KEY ,
      name TEXT NOT NULL,
      age  INTEGER NOT NULL
    )
  """.update.run

  def update1(key: String, name: String, age: Int): Update0 =
    sql"update person set name = $name, age  = $age where key = $key".update

  def insert1(key: String, name: String, age: Int): Update0 =
    sql"insert into person (key, name, age) values ($key, $name, $age)".update

  def select1(key: String): Query0[Person] =
    sql"select name, age from person where key = ${key}".query[Person]

}

class SQLiteStore[F[_]: Sync](val xa: Transactor[F])
    extends PersonStoreAlgebra[F] {

  import PersonStoreDAO._

  def reset = (drop, create).mapN(_ + _).transact(xa)

  def get(key: String): F[Either[String, Person]] =
    select1(key)
      .to[List]
      .transact(xa)
      .map(_.headOption)
      .map(_.toRight(s"NotFound: $key"))

  def set(key: String, value: Person): F[Either[String, Unit]] =
    for {
      res <- get(key)
      _ <- if (res.isRight)
        update1(key, value.name, value.age).run.transact(xa)
      else
        insert1(key, value.name, value.age).run.transact(xa)
    } yield Right(())

}
