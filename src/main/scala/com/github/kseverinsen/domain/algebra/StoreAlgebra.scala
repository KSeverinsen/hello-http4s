package com.github.kseverinsen.domain.algebra

trait StoreAlgebra[F[_], K, V, E] {
  def get(key: K): F[Either[E, V]]
  def set(key: K, value: V): F[Either[E, Unit]]
}
