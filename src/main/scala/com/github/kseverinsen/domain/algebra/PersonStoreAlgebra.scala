package com.github.kseverinsen.domain.algebra

import com.github.kseverinsen.domain.types.Person

trait PersonStoreAlgebra[F[_]] extends StoreAlgebra[F, String, Person, String]
