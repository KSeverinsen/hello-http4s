package com.github.kseverinsen.domain.types

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto._

case class Person(name: String, age: Int)

object Person {
  implicit val jsonDecoder: Decoder[Person] = deriveDecoder
  implicit val jsonEncoder: Encoder[Person] = deriveEncoder
}
