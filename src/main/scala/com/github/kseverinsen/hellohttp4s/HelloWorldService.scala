package com.github.kseverinsen.hellohttp4s

import cats.implicits._
import cats.effect.Effect
import com.github.kseverinsen.domain.algebra.PersonStoreAlgebra
import com.github.kseverinsen.domain.types.Person
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import org.http4s.circe.CirceEntityCodec._

class HelloWorldService[F[_]: Effect](S: PersonStoreAlgebra[F])
    extends Http4sDsl[F] {

  val service: HttpRoutes[F] = {
    HttpRoutes.of[F] {

      case GET -> Root / "kvstore" / key =>
        S.get(key).flatMap {
          case Left(err) => NotFound(err)
          case Right(a)  => Ok(a)
        }

      case req @ POST -> Root / "kvstore" / key =>
        (for {
          json <- req.as[Person]
          res <- S.set(key, json)
        } yield res).attempt.flatMap {
          case Right(_) => Ok()
          case Left(_)  => BadRequest()
        }

    }
  }
}
