package com.github.kseverinsen.hellohttp4s

import cats.effect.{ContextShift, ExitCode, IO, IOApp}

import com.github.kseverinsen.infrastructure.interpreters.SQLiteStore
import cats.implicits._
import doobie.util.transactor.Transactor
import org.http4s.server.blaze._
import org.http4s.implicits._
import org.http4s.server.Router

import scala.concurrent.ExecutionContext

object Main extends IOApp {

  implicit val cs: ContextShift[IO] = IO.contextShift(ExecutionContext.global)

  val dbTransactor: Transactor[IO] =
    Transactor.fromDriverManager[IO](
      "org.sqlite.JDBC",
      "jdbc:sqlite:sample.db",
      "",
      ""
    )

  val storeInterpreter =
    new SQLiteStore[IO](dbTransactor)

  val foo = storeInterpreter.reset.unsafeRunSync()

  val helloWorldService =
    new HelloWorldService(storeInterpreter)

  val httpApp = Router("/" -> helloWorldService.service).orNotFound

  def run(args: List[String]): IO[ExitCode] =
    BlazeServerBuilder[IO]
      .bindHttp(8080, "0.0.0.0")
      .withHttpApp(httpApp)
      .resource
      .use(_ => IO.never)
      .as(ExitCode.Success)

}
