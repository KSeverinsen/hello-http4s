val Http4sVersion = "0.20.0"
val Specs2Version = "4.1.0"
val LogbackVersion = "1.2.3"
val CatsVersion = "1.6.0"

val circeVersion = "0.10.0"

lazy val root = (project in file("."))
  .settings(
    organization := "com.github.kseverinsen",
    name := "hello-http4s",
    version := "0.0.1-SNAPSHOT",
    scalaVersion := "2.12.8",
    libraryDependencies ++= Seq(
      "org.http4s" %% "http4s-blaze-server" % Http4sVersion,
      "org.http4s" %% "http4s-circe" % Http4sVersion,
      "org.http4s" %% "http4s-dsl" % Http4sVersion,
      "org.specs2" %% "specs2-core" % Specs2Version % "test",
      "ch.qos.logback" % "logback-classic" % LogbackVersion,
      "org.typelevel" %% "cats-core" % CatsVersion,
      "io.circe" %% "circe-generic" % circeVersion,
      "org.tpolecat" %% "doobie-core" % "0.6.0",
      "org.tpolecat" %% "doobie-hikari" % "0.6.0", // HikariCP transactor.
      "org.tpolecat" %% "doobie-specs2" % "0.6.0", // Specs2 support for typechecking statements.
      "org.tpolecat" %% "doobie-scalatest" % "0.6.0", // ScalaTest support for typechecking statements.
      "org.xerial" % "sqlite-jdbc" % "3.8.11.2"
    ),
    addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.6"),
    addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.2.4"),
    wartremoverErrors ++= Warts.unsafe,
    scalacOptions ++= Seq(
      // "-encoding", "utf8", // Option and arguments on same line
      // "-Xfatal-warnings",  // New lines for each options
      // "-deprecation",
      // "-unchecked",
      // "-language:implicitConversions",
      "-language:higherKinds",
      // "-language:existentials",
      // "-language:postfixOps",
      "-Ypartial-unification"
    )
  )
